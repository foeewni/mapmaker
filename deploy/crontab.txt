*/30 7-21 * * * (cd /opt/mapserver/; ./env/bin/python ./manage.py geocode_usersubmissions >> ./logs/cronjobs.log 2>&1)
*/5 7-21 * * * (cd /opt/mapserver/; ./env/bin/python ./manage.py generate_feeds >>./logs/generate_feeds.log 2>&1)
*/30 7-21 * * * (cd /opt/mapserver/; ./env/bin/python ./manage.py bees_events_feed_proxy >> ./logs/bees_events_feed_proxy.log 2>&1)
0 1 * * * (cd /opt/mapserver/; ./env/bin/python ./manage.py cleanup_mail --days=30 >> ./logs/cron_mail.log 2>&1)