# Default settings.
# Override environment or deployment specific values in settings_local.py which is imported last
# e.g. link or move one of the settings_local.*.template.py to settings_local.py
# Keep settings_local.py in .gitignore

ALLOWED_HOSTS = ['mapmaker.example.com']
SECRET_KEY = 'REPLACE_ME'  # Generate with ./manage.py generate_secret_key
ADMINS = (('Admin', 'admin@example.com'),)
MANAGERS = ADMINS


# Paths ##########################################################################################################
from os.path import abspath, dirname, join
SITE_ROOT = abspath(dirname(dirname(dirname(__file__))))
STATIC_ROOT = join(SITE_ROOT, 'static')
MEDIA_ROOT = join(SITE_ROOT, 'files')


# URLs #################################################################################################################
URL_PREFIX = ''
MEDIA_URL = URL_PREFIX + '/files/'
STATIC_URL = URL_PREFIX + '/static/'
LOGIN_URL = '/maps/login/'  # TODO After upgrade to 1.5, replace with named url pattern for DRYness: 'django.contrib.auth.views.login'


# Feed Output ##########################################################################################################
FEED_OUTPUT_DIR = join(SITE_ROOT, 'feeds')
FEED_URL = '/' + URL_PREFIX + 'maps/feeds/'


# Map Publishing #######################################################################################################
MAP_PUBLISH_DIR = join(SITE_ROOT, 'maps')
MAP_PUBLISH_ROOT_URL = "http://maps.example.com/"


# Geocoding ############################################################################################################
GEOCODE_URL = 'http://mapit.mysociety.org/postcode/'  # N.B. usage limits: http://mapit.mysociety.org/#usage-licence
GEOCODE_MAX_ATTEMPTS = 5


# Database #############################################################################################################
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(SITE_ROOT, 'db/db.sqlite')
    }
}

# Other Django Settings ################################################################################################
DEBUG = False
TEMPLATE_DEBUG = DEBUG

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/London'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False  # MSSQL not supporting it

# Additional locations of static files
STATICFILES_DIRS = (
# Put strings here, like "/home/html/static" or "C:/www/django/static".
# Always use forward slashes, even on Windows. Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'map_server.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'map_server.wsgi.application'


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'south',
    'django_extensions',
    'map_server',
)


# Rest Framework #######################################################################################################
INSTALLED_APPS += ('rest_framework',)
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.TemplateHTMLRenderer',
        'map_server.renderers.PlainTextJSONRenderer',
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': ('rest_framework.authentication.SessionAuthentication',)
}


# Logging ##############################################################################################################
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'},
        'simple': {'format': '%(levelname)s %(message)s'},
    },
    'handlers': {
        'default':
            {'level': 'WARNING',
             'class': 'logging.handlers.RotatingFileHandler',
             'filename': join(SITE_ROOT, 'logs/site.log'),
             'maxBytes': 1024 * 1024 * 1, # 1 MB
             'backupCount': 0,
             'formatter': 'verbose',
            },
        'request_handler':
            {'level': 'WARNING',
             'class': 'logging.handlers.RotatingFileHandler',
             'filename': join(SITE_ROOT, 'logs/request.log'),
             'maxBytes': 1024 * 1024 * 1,
             'backupCount': 0,
             'formatter': 'verbose',
            },
    },
    'loggers': {
        '':
            {'handlers': ['default'],
             'level': 'WARNING',
             'propagate': True
            },
        'django.request': # Stop SQL debug from logging to main logger
            {'handlers': ['request_handler'],
             'level': 'WARNING',
             'propagate': False
            },
        'django.db.backends': # Stop SQL debug from logging to main logger
            {'handlers': ['request_handler'],
             'level': 'WARNING',
             'propagate': False
            },
    }
}


try:
    from settings_local import *
except ImportError:
    pass