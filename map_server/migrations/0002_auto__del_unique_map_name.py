# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Map', fields ['name']
        db.delete_unique(u'map_server_map', ['name'])


    def backwards(self, orm):
        # Adding unique constraint on 'Map', fields ['name']
        db.create_unique(u'map_server_map', ['name'])


    models = {
        u'map_server.exclusionbox': {
            'Meta': {'object_name': 'ExclusionBox'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat_1': ('django.db.models.fields.DecimalField', [], {'max_digits': '7', 'decimal_places': '5'}),
            'lat_2': ('django.db.models.fields.DecimalField', [], {'max_digits': '7', 'decimal_places': '5'}),
            'lon_1': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '5'}),
            'lon_2': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '5'})
        },
        u'map_server.layer': {
            'Meta': {'ordering': "['id']", 'object_name': 'Layer'},
            'cluster_icon': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'layer_cluster_icons'", 'null': 'True', 'to': u"orm['map_server.MarkerIcon']"}),
            'cluster_text_color': ('django.db.models.fields.CharField', [], {'default': "'000000'", 'max_length': '6'}),
            'datasets': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['map_server.LayerDataset']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legend_icon': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'layer_legend_icon'", 'null': 'True', 'to': u"orm['map_server.MarkerIcon']"}),
            'marker_icons': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['map_server.MarkerIcon']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'New Layer'", 'max_length': '64'}),
            'should_cluster': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'visible': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'z_index': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'map_server.layerdataset': {
            'Meta': {'object_name': 'LayerDataset'},
            'decimal_precision': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '3'}),
            'field_override': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'fusion_styles': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_render_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'latitude_field': ('django.db.models.fields.CharField', [], {'default': "'lat'", 'max_length': '64'}),
            'longitude_field': ('django.db.models.fields.CharField', [], {'default': "'lng'", 'max_length': '64'}),
            'max_result_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'New Dataset'", 'max_length': '64'}),
            'parser': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'popup_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'render_frequency_in_minutes': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '30'}),
            'render_location': ('django.db.models.fields.CharField', [], {'default': "'local'", 'max_length': '16'}),
            'render_status': ('django.db.models.fields.CharField', [], {'default': "'waiting'", 'max_length': '16'}),
            'source_location': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'source_type': ('django.db.models.fields.CharField', [], {'default': "'fusion'", 'max_length': '16'})
        },
        u'map_server.map': {
            'Meta': {'object_name': 'Map'},
            'accept_submissions': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'center_latitude': ('django.db.models.fields.DecimalField', [], {'default': '54.7', 'max_digits': '10', 'decimal_places': '7'}),
            'center_longitude': ('django.db.models.fields.DecimalField', [], {'default': '-2.0', 'max_digits': '10', 'decimal_places': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'initial_zoom': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '6'}),
            'layers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['map_server.Layer']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'New Map'", 'max_length': '64'}),
            'published_url': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'shareable_url_root': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'specific_css': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'specific_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'specific_js': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'submission_header_html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'tile_set': ('django.db.models.fields.CharField', [], {'default': "'google.maps.MapTypeId.TERRAIN'", 'max_length': '32'})
        },
        u'map_server.mapdatasource': {
            'Meta': {'ordering': "['-dated']", 'object_name': 'MapDataSource', 'db_table': "'action_master_for_maps'", 'managed': 'False'},
            'action_id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'}),
            'dated': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'postcode': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'source_code': ('django.db.models.fields.CharField', [], {'max_length': '31'}),
            'x_coord': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'y_coord': ('django.db.models.fields.CharField', [], {'max_length': '7'})
        },
        u'map_server.markericon': {
            'Meta': {'object_name': 'MarkerIcon'},
            'icon': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'})
        },
        u'map_server.usersubmission': {
            'Meta': {'object_name': 'UserSubmission'},
            'approval_email_sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'approval_status': ('django.db.models.fields.CharField', [], {'default': "'pending'", 'max_length': '16'}),
            'color': ('django.db.models.fields.CharField', [], {'max_length': '6', 'blank': 'True'}),
            'data_protection_can_send': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'firstname': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'geocoding_attempts': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastname': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'map': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['map_server.Map']"}),
            'photo': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'postcode': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'})
        }
    }

    complete_apps = ['map_server']