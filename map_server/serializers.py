from rest_framework import serializers
from map_server.models import UserSubmission, Map, Layer, LayerDataset, MarkerIcon


class UserSubmissionSerializer(serializers.ModelSerializer):
    map = serializers.PrimaryKeyRelatedField(required=False)
    photo = serializers.FileField(max_length=100, required=False)
    latitude = serializers.FloatField(required=False)
    longitude = serializers.FloatField(required=False)
    firstname = serializers.CharField(max_length=64, required=False)
    lastname = serializers.CharField(max_length=64, required=False)
    color = serializers.CharField(max_length=6, required=False)

    class Meta:
        model = UserSubmission
        fields = ('id', 'map', 'photo', 'latitude', 'longitude', 'description', 'firstname', 'lastname', 'email',
                  'postcode', 'color', 'approval_status', 'uid', 'data_protection_can_send')


class UserSubmissionPublicSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserSubmission
        fields = ('photo', 'latitude', 'longitude', 'description', 'color', 'approval_status', 'uid')


class MapSerializer(serializers.ModelSerializer):
    class Meta:
        model = Map


class LayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Layer


class LayerDatasetSerializer(serializers.ModelSerializer):
    class Meta:
        model = LayerDataset
        exclude = ('last_render_time', 'render_status')


class MarkerIconSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarkerIcon
